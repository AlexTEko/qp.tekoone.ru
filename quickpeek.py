from instagram.client import InstagramAPI
from instagram.bind import InstagramAPIError
import MySQLdb
import time
import datetime as dt
import vk

conn = MySQLdb.connect(host= "localhost", user="qp", passwd="ZzUKMTYJ4HaCYYKc", db="qp")
x = conn.cursor()

apiI = InstagramAPI(client_id='755d59a5fc5e4d318967945c0e58de5c', client_secret='67007e229871404f907303b49aa15487')
session = vk.Session()
apiVK = vk.API(session,v=5.37)

def api_request(request):
    try:
        millis=int(round(time.time()*1000))
        x.execute("INSERT INTO api_calls(request,microtime) VALUES (%s,%s)",(request,millis))
    except MySQLdb.Error as e:
        conn.rollback()              #rollback transaction here
		#print e[1]

def media_search_inst(count, lat, lng, distance,timelimit):
	try:
		search_media = apiI.media_search(count=count,lat=lat,lng=lng,distance = distance,min_timestamp = timelimit)
		#api_request("media_search(" + lat + "," + lng +")")
	except InstagramAPIError as e:
		if (e.status_code == 400):
			print "\nUser is set to private."
	return search_media

def media_search_vk(lat, lng, distance, timelimit):
    try:
        search_media = apiVK.photos.search(lat=lat,long=lng,count=1000,radius=distance,start_time=timelimit)
    except InstagramAPIError as e:
        if (e.status_code == 400):
            print "\nUser is set to private."
    return search_media

def mysql_insert_inst(search_media,search_id):
	try:
		# x.execute("SELECT photo_id FROM search_media WHERE search_id = %s ORDER BY media_id DESC LIMIT 1",(search_id))
		# if x.rowcount == 1:
			# last_id = x.fetchone()[0]
		# else:
			# last_id = 0
		for media in search_media:
			id = media.id
			created_time = media.created_time
			low_resolution = media.images['low_resolution'].url
			standard_resolution = media.images['standard_resolution'].url
			thumbnail = media.images['thumbnail'].url
			link = media.link
			lat = media.location.point.latitude
			lng = media.location.point.longitude
			#print created_time
			#print media.location.point.longitude
			#print low_resolution
			#x.execute("SELECT photo_id FROM search_media WHERE search_id = %s AND photo_id = %s",(search_id, id))
			#if x.rowcount == 0:
				#print 'last_id='+last_id +' id='+id
			x.execute("INSERT IGNORE INTO search_media_inst(photo_id,created_time,low_resolution,standard_resolution,thumbnail,link,search_id,lat,lng) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s)",(id,created_time,low_resolution,standard_resolution,thumbnail,link,search_id,lat,lng))
			conn.commit()
			#else:
		#		break
			#print media.link
	except MySQLdb.Error as e:
		conn.rollback()              #rollback transaction here
		print e[1]

def mysql_insert_vk(search_media,search_id):
    try:
        for media in search_media['items']:
            id = media['id']
            album_id = media['album_id']
            owner_id = media['owner_id']
            photo_75 = media['photo_75']
            photo_130 = '';
            photo_604 = '';
            photo_807 = '';
            photo_1280 = '';
            photo_2560 = '';
            if 'photo_130' in media:
                photo_130 = media['photo_130']
            if 'photo_604' in media:
                photo_604 = media['photo_604']
            if 'photo_807' in media:
                photo_807 = media['photo_807']
            if 'photo_1280' in media:
                photo_1280 = media['photo_1280']
            if 'photo_2560' in media:
                photo_2560 = media['photo_2560']
            width = media['width']
            height = media['height']
            text = media['text']
            date = media['date']
            lat = media['lat']
            lng = media['long']
            x.execute("INSERT IGNORE INTO search_media_vk(id,owner_id,search_id,photo_75,photo_130,photo_604,photo_807,photo_1280,photo_2560,date) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",(id,owner_id,search_id,photo_75,photo_130,photo_604,photo_807,photo_1280,photo_2560,date))
            conn.commit()
    except MySQLdb.Error as e:
        conn.rollback()              #rollback transaction here
        print e[1]

def mysql_insert2():
	x.execute("INSERT INTO updates(object) VALUES ('1')")

#=======================================================================================================================
n1=dt.datetime.now()
#=======================================================================================================================
x.execute("SELECT * FROM searches WHERE state = 1 order by time DESC LIMIT 10")
subs =  x.fetchall()

for sub in subs:
    if (sub):
        api_request("media_search_inst("+ str(sub[0]) +")")
        mysql_insert_inst(media_search_inst(sub[4],sub[1],sub[2],sub[3],sub[5]),sub[0])
        api_request("media_search_vk("+ str(sub[0]) +")")
        mysql_insert_vk(media_search_vk(sub[1],sub[2],sub[3],sub[5]),sub[0])
        #print media_search_vk(sub[1],sub[2],sub[3],sub[5])['items'][0]['id']
	#print media_search(sub[4],sub[1],sub[2],sub[3],sub[5])
		#time.sleep(1)

x.close()
conn.close()
#=======================================================================================================================
n2=dt.datetime.now()
#=======================================================================================================================
#print (n2-n1)
