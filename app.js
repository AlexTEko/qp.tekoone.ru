'use strict';

L.mapbox.accessToken = 'pk.eyJ1IjoidGVrb29uZSIsImEiOiI1OTYzZTZiZTJiMTcwMDhlODQ1ZDZmMzVhNDhhMmNmNCJ9.l30FE0AfWHbddGvH7kDgPw';

var app = angular.module('QuickPeek', ['ui.router','ui.bootstrap','angular-loading-bar']);

app.config(function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/map');
    $stateProvider
        .state('map', {
            url: '/map',
            templateUrl: 'partial/partial-map.html',
            controller: 'mapController'
        })
        .state('subs', {
            url: '/subs/:id',
            templateUrl: 'partial/partial-subs.html',
            controller: 'subsController'
        })
        .state('admin', {
	        url: '/admin',
	        templateUrl: 'partial/partial-admin.html',
            controller: 'adminController'
        });
});

app.directive('errSrc', function() {
    return {
        link: function(scope, element, attrs) {
            element.bind('error', function() {
                if (attrs.src != attrs.errSrc) {
                    attrs.$set('src', attrs.errSrc);
                }
            });
        }
    }
});

app.controller('mapController', function($scope, $http) {

    $scope.myloc = {
        "lat": '',
        "lng": '',
        "radius": '',
        "hours": '',
        "origin": 1
    }

    $scope.alerts = [];

    $scope.addAlert = function(msg, link) {
        $scope.alerts.push({type: 'info', msg: msg, link: link});
    };

    $scope.closeAlert = function(index) {
        $scope.alerts.splice(index, 1);
    };

    $scope.myloc_ready = function(index) {
        if (($scope.myloc.lat != '') && ($scope.myloc.lng != '') && ($scope.myloc.radius != '') && ($scope.myloc.hours != ''))
            return true
        return false
    };

    function getLocation() {
      if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(locateMe);
      } else {
          x.innerHTML = "Geolocation is not supported by this browser.";
      }
    }

    function locateMe(position) {
      if (position)
        map.setView([position.coords.latitude, position.coords.longitude], 9)
    }

    function sub(location) {
        $http({
            method: 'POST',
            url: 'http://qp.tekoone.ru/api.php?get=sub',
            data: location,
            headers: { 'Content-Type': 'application/json' }
        }).success(function (response) {
            //console.log(response);
            //$scope.addAlert('Subscribed! Follow <a href="http://qp.tekoone.ru/#/subs/' + response.id + '" class="alert-link">this</a> ');
            if (response.success) {
              $scope.addAlert('Success!','/#/subs/' + response.success.short);
              $scope.myloc = {
                  "lat": '',
                  "lng": '',
                  "radius": '',
                  "hours": '',
                  "origin": 1
              }
            }
        })
    }

    var map = L.mapbox.map('map', 'tekoone.a578da75').addControl(L.mapbox.geocoderControl('mapbox.places'));
    getLocation();
    var marker;
    var circle;

    var circleRadius = function() {
        if (circle) {
            circle.setRadius(r.getValue())
            $scope.myloc.radius = r.getValue();
            $scope.$apply();
            //console.log($scope.myloc.radius);
        }
    };

    var hours = function() {
        if ($scope.myloc) {
            $scope.myloc.hours = h.getValue();
            $scope.$apply();
        }
    };

    var r = $('#R').slider()
        .on('slide', circleRadius)
        .data('slider');
    var h = $('#H').slider()
        .on('slide', hours)
        .data('slider');

    map.on('click', function(e) {
        if (!marker) {
            marker = L.marker(e.latlng).addTo(map);
            circle = L.circle(e.latlng, 500).addTo(map);
        }
        else {
            marker.setLatLng(e.latlng);
            circle.setLatLng(e.latlng);
        }
        $scope.myloc.lat = e.latlng.lat;
        $scope.myloc.lng = e.latlng.lng;
        $scope.$apply();
    });

    $scope.subscribe = function() {
        if ($scope.myloc) {
            sub($scope.myloc);
        }
    }
});

app.controller('subsController',function($scope, $http,$stateParams ) {

    $scope.filteredPhotos = []
    $scope.currentPage = 1
    $scope.numPerPage = 35
    $scope.maxSize = 3;

    $scope.photos = [];

    var map = L.mapbox.map('map', 'tekoone.a578da75').addControl(L.mapbox.geocoderControl('mapbox.places'));

    function sub_map(id) {
        $http.get("http://qp.tekoone.ru/api.php", {
            params: {
                subid: id
            }
        }).success(function (response) {
          //  console.log(response);
            if (response) {
              L.circle([response.lat, response.lng], response.distance).addTo(map);
              map.setView([response.lat, response.lng], 11);
            }
        });
    }

    sub_map($stateParams.id);

    function sub_active(id) {
        $http.get("http://qp.tekoone.ru/api.php", {
            params: {
                subid: id
            }
        }).success(function (response) {
          //  console.log(response);
            if (response.data) {
                if ($scope.photos.length != response.data.length) {
                  $scope.photos = response.data;
                  $scope.subid = response.id;
                  $scope.$watch('currentPage + numPerPage', function () {
                      var begin = (($scope.currentPage - 1) * $scope.numPerPage)
                          , end = begin + $scope.numPerPage;

                      $scope.filteredPhotos = $scope.photos.slice(begin, end);
                  });
                }
            }
        });
    }

    //console.log($stateParams.id);
    sub_active($stateParams.id);
    setInterval(function () {sub_active($stateParams.id)},10000);
});

app.controller('adminController',function($scope, $http) {
	$scope.subs = [];
	$scope.filteredSubs = []
        ,$scope.currentPage = 1
        ,$scope.numPerPage = 10
        ,$scope.maxSize = 5;

  var getList = function() {
    $http.get("api.php", {
    			params: {
    				get: 'status'
    			}
    		}).success(function (response) {
    $(function () {
      $('#chart').highcharts({
          title: {
              text: 'API Requests Count',
              x: -20 //center
          },
          subtitle: {
              text: 'Requests per hour',
              x: -20
          },
          xAxis: {
              //type: 'timeline',
              categories: response.records.date
          },
          yAxis: {
              title: {
                  text: 'requests'
              },
              plotLines: [{
                  value: 0,
                  width: 1,
                  color: '#808080'
              }]
          },
          tooltip: {
              valueSuffix: ' requests'
          },
          series: [{
                  type: 'area',
                  name: 'Requests per hour',
                  data: response.records.req
              }]
        });
      });
    });
  };

	var getSubs = function() {
		$http.get("http://qp.tekoone.ru/api.php?get=subs", { ignoreLoadingBar: true }).success(function(response) {
			//console.log(response);
      if ($scope.subs != response) {
  			$scope.subs = response;
  			$scope.$watch('currentPage + numPerPage', function () {
                  var begin = (($scope.currentPage - 1) * $scope.numPerPage)
                      , end = begin + $scope.numPerPage;

                  $scope.filteredSubs = $scope.subs.slice(begin, end);
              });
      }
		})
	};

	$scope.unSub = function(id) {
		//console.log(id);
		$http.get("http://qp.tekoone.ru/api.php?get=unsub&id="+id).success(function(response) {
			getSubs();
		})
	}

	getSubs();
  getList();
	setInterval(function () {getSubs()}, 10000);
  setInterval(function () {getList()}, 60000);
});
