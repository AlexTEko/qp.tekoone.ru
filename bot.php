<?php
  header("Content-Type: application/json; charset=UTF-8");
  $conn = new mysqli("localhost", "qp", "ZzUKMTYJ4HaCYYKc", "qp");
  $token = '106457625:AAHLqNcMUh6AVx_4hF0JQYz5l9_O1HUdabI';
  $DEBUG = true;

  //$banned = array('48769646','96922108','8613805','83538801','37843490','5585011','108485733');
  $banned = [];

  function sendMessage($id,$message,$keyboard='') {
    global $token;
    file_get_contents('https://api.telegram.org/bot'.$token.'/sendMessage?chat_id='
    .$id.'&text='
    .urlencode($message).'&reply_markup='
    .urlencode($keyboard));
  }

  function toLog($raw) {
    global $DEBUG;
    if ($DEBUG) {
      $fp = fopen('/var/www/qp.tekoone.ru/bot_debug.txt', 'a+');
      fwrite($fp, "\n\r".$raw);
      fclose($fp);
    }
  }

  function sendChatAction($id,$action='typing') {
    global $token;
    file_get_contents('https://api.telegram.org/bot'.$token.'/sendChatAction?chat_id='
    .$id.'&action='
    .$action);
  }

  function newSub($from) {
    global $conn;
    $conn->query("DELETE FROM searches_bot WHERE from_id = $from");
    $conn->query("INSERT INTO searches_bot(from_id, state) VALUES ($from,0)");
  }

  function setState($from,$state) {
    global $conn;
    $conn->query("UPDATE searches_bot SET state=$state WHERE from_id = $from");
  }

  function getState($from) {
    global $conn;
    $state = 0;
    $result = $conn->query("SELECT state FROM searches_bot WHERE from_id = $from");
    if ($result->num_rows > 0)
     $state = $result->fetch_assoc()['state'];
    return $state;
  }

  function setRadius($from,$radius) {
    if (($radius < 100) or ($radius > 5001))
      $radius = 5000;
    global $conn;
    $conn->query("UPDATE searches_bot SET radius=$radius WHERE from_id = $from");
  }

  function setLocation($from,$lng,$lat) {
    global $conn;
    $conn->query("UPDATE searches_bot SET lng=$lng,lat=$lat WHERE from_id = $from");
  }

  function setTime($from,$time) {
    if (($time < 1) or ($time > 48))
      $time = 48;
    global $conn;
    $conn->query("UPDATE searches_bot SET time=$time WHERE from_id = $from");
  }

  function subscribe($from) {
    global $conn;
    $search = 0;
    $result = $conn->query("SELECT * FROM searches_bot WHERE from_id = $from");
    if ($result->num_rows > 0) {
      $search = $result->fetch_assoc();

      $url = "http://qp.tekoone.ru/api.php?get=sub";    //sub($array['lat'], $array['lng'], $array['radius'], $array['hours']);
      $content = json_encode(array('lat' => $search['lat'], 'lng' => $search['lng'], 'radius' => $search['radius'], 'hours' => $search['time'], 'origin' => $from));

      $curl = curl_init($url);
      curl_setopt($curl, CURLOPT_HEADER, false);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($curl, CURLOPT_HTTPHEADER,array("Content-type: application/json"));
      curl_setopt($curl, CURLOPT_POST, true);
      curl_setopt($curl, CURLOPT_POSTFIELDS, $content);

      $json_response = curl_exec($curl);

      $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

      curl_close($curl);

      $response = json_decode($json_response, true);
      return $response['success']['short'];
    }
  }

  $raw = file_get_contents('php://input');
  $gram=json_decode($raw,true);

  if (in_array($gram['message']['from']['id'], $banned))
    exit('{}');

  $fp = fopen('/var/www/qp.tekoone.ru/update.id', 'r+');
  $update_id = fread($fp,filesize("/var/www/qp.tekoone.ru/update.id"));
  if ($update_id < $gram['update_id']) {
    fseek($fp,0);
    fwrite($fp, $gram['update_id']);
    fclose($fp);
  }
  else
    exit('{}');

  toLog($raw);
  $from = $gram['message']['from']['id'];

  if (isset($gram['message']['location'])) {
    //save $gram['message']['location']['longitude'] $gram['message']['location']['latitude'] set state = 1
    setLocation($from, $gram['message']['location']['longitude'], $gram['message']['location']['latitude']);
    setState($from, 1);
    sendMessage($from,"Ок. Какой радиус поиска в метрах? (не более 5000)\n");
  }

  if (isset($gram['message']['text'])) {
    $msg = $gram['message']['text'];
    if ($msg == "/start") {
      sendMessage($from,"Привет, ".$gram['message']['chat']['first_name']."! Давай я найду фото в заданном местоположении. Для начала отправь /newsub.\n");
    }
    elseif ($msg == "/newsub") {
      sendMessage($from,"Ок, новый поиск. Где искать?\n");
      newSub($from);
    }
    elseif (getState($from) == 0) {
      sendMessage($from,"Ок, новый поиск. Где искать?\n");
      newSub($from);
    }
    elseif (getState($from) == 1) {
      //save radius set state = 2
      if (is_numeric($gram['message']['text']) and ($gram['message']['text'] < 5001)) {
        setRadius($from, $gram['message']['text']);
        setState($from, 2);
        sendMessage($from,"Ок. Сколько часов в прошлом? (не более 48 часов)\n");
      }
      else {
        sendMessage($from,"Эээ...не понял\n");
      }
    }
    elseif (getState($from) == 2) {
      //save hours set state = 3
      if (is_numeric($gram['message']['text']) and ($gram['message']['text'] < 49)) {
        setTime($from, $gram['message']['text']);
        setState($from, 3);
      }
      else {
        sendMessage($from,"Эээ...не понял\n");
      }
    }
  }

  if (getState($from) == 3) {
    //Subscribe and state = 0
    if ($id = subscribe($from)) {
      setState($from, 0);
      sendMessage($from,"Все готово, смотреть фото можно по ссылке (необходимо немного подождать)\nhttp://qp.tekoone.ru/#/subs/$id\n");
    }
  }
$conn->close();
 ?>
