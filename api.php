<?php
require ('mysql.php');

$client_id = '755d59a5fc5e4d318967945c0e58de5c';
$client_secret = '67007e229871404f907303b49aa15487';

$work_dir = getcwd();

header("Access-Control-Allow-Origin: *");
header('Content-Type: application/json; charset=utf-8');
ob_start('ob_gzhandler');

function generateRandomString($length = 10) {
    return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
}

function toCache($id,$json) {
  global $work_dir;
  if ($fh = fopen($work_dir.'/cache/'.$id.'.media', 'w')) {
    fwrite($fh, json_encode($json));
    fclose($fh);
  }
}

function fromCache($id) {
  global $work_dir;
  if (file_exists($work_dir.'/cache/'.$id.'.media'))
    return file_get_contents($work_dir.'/cache/'.$id.'.media');
}

function sub($lat,$lng,$radius,$hours = 48, $origin = 1) {
    global $conn;
    $timestamp = time() - 3600*$hours;
    //$conn->query("UPDATE searches SET state = 0 ");
    if ($origin > 2) {
      $conn->query("UPDATE searches SET state = 0 WHERE origin = $origin");
    }
    $url = generateRandomString(16);
    $conn->query("INSERT INTO searches(lat, lng, distance, count, timelimit, origin, short) VALUES ($lat,$lng,$radius,'100',$timestamp, $origin,'$url' )") or die($conn->error);
    $result = $conn->query("SELECT short FROM searches ORDER BY id DESC LIMIT 1");
    $res = $result->fetch_assoc();
    return $res;
}   //Subscribe on location

function view_sub($sub_id) {
    global $conn,$work_dir;
    $pics = [];
    $media_count = 0;

    if ($sub_id == 'active') {
        $result = $conn->query("SELECT id FROM searches WHERE state = 1 ");
        $row = $result->fetch_assoc();
        $id = $row['id'];
    }
    else
        $id = $sub_id;

    $result = $conn->query("SELECT id FROM searches WHERE short = '$id' ");
    $row = $result->fetch_assoc();
    $id = $row['id'];

    $result = $conn->query("SELECT COUNT(*) as c FROM search_media_inst WHERE search_id = $id");
    $row1 = $result->fetch_assoc();
    $result = $conn->query("SELECT COUNT(*) as c FROM search_media_vk WHERE search_id = $id");
    $row2 = $result->fetch_assoc();
    $count = $row1['c']+$row2['c'];
    if ($count > 0) {
      if ($count>10000)
        $count = 10000;
      if (file_exists($work_dir.'/cache/'.$id.'.count'))
          $media_count = file_get_contents($work_dir.'/cache/'.$id.'.count');

      if ($count != $media_count) {
        if ($fh = fopen($work_dir.'/cache/'.$id.'.count', 'w')) {
          fwrite($fh, $count);
          fclose($fh);
        }

        $result = $conn->query("SELECT photo_id as id,link,thumbnail,UNIX_TIMESTAMP(STR_TO_DATE(created_time, '%Y-%c-%d %H:%i:%s')) as date FROM `search_media_inst` WHERE search_id = $id
                                UNION
                                SELECT id,owner_id,photo_130,date FROM search_media_vk WHERE search_id = $id
                                ORDER BY date DESC LIMIT 10000");
        while($row = $result->fetch_assoc()) {
          //  $pic['low_resolution'] = $row['low_resolution'];
          //  $pic['standard_resolution'] = $row['standard_resolution'];
          if (is_numeric($row['link'])) {
            $pic['link'] = 'https://vk.com/photo'.$row['link'].'_'.$row['id'];
          }
          else {
            $pic['link'] = $row['link'];
          }
          $pic['thumbnail'] = $row['thumbnail'];
          $pic['dtime'] = $row['date'];
          $pics['data'][] = $pic;
        }

        $pics['id'] = $id;

        $result = $conn->query("SELECT * FROM searches WHERE id = $id");
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                $pics['lat'] = $row['lat'];
                $pics['lng'] = $row['lng'];
                $pics['distance'] = $row['distance'];
            }
        }
        toCache($id, $pics);
      } else {
        return fromCache($id);
      }
    }
    return json_encode($pics);
}   //View specific subscription on location

function view_subs() {
    global $conn;
    $result = $conn->query("SELECT id, short, lat, lng, distance, time, state, origin, media.count, media_vk.count as count_vk FROM searches
      left JOIN ( SELECT search_id, COUNT(*) as count FROM search_media_inst as media GROUP BY search_id ) as media on searches.id = media.search_id
      left JOIN ( SELECT search_id, COUNT(*) as count FROM search_media_vk as media_vk GROUP BY search_id ) as media_vk on searches.id = media_vk.search_id ORDER BY state DESC, time DESC");
    while ($row = $result->fetch_assoc()) {
	    $temp_row = [];
        $temp_row['id'] = $row['id'];
        $temp_row['short'] = $row['short'];
        $temp_row['lat'] = $row['lat'];
        $temp_row['lng'] = $row['lng'];
        $temp_row['distance'] = $row['distance'];
        $temp_row['time'] = $row['time'];
        if ($row['origin'] == 1)
          $temp_row['origin'] = 'web';
        elseif ($row['origin'] == 2)
          $temp_row['origin'] = 'pebble';
        else
          $temp_row['origin'] = $row['origin'];
        if ($row['count'])
          $temp_row['count'] = $row['count'];
        else
          $temp_row['count'] = 0;
        if ($row['count_vk'])
          $temp_row['count_vk'] = $row['count_vk'];
        else
          $temp_row['count_vk'] = 0;
        if ($row['state'] == 1)
        	$temp_row['state'] = $row['state'];
        $rows[] = $temp_row;
    }
    return $rows;
}   //View all subs

function unsub($id) {
    global $conn;
    $conn->query("UPDATE searches SET state = 0 WHERE id = $id");
}   //Stop active subscription

function NotifyBot($value) {
  $curl = curl_init();
  curl_setopt_array($curl, array(
      CURLOPT_RETURNTRANSFER => 1,
      CURLOPT_URL => 'http://notic.tekoone.ru/send',
      CURLOPT_POST => 1,
      CURLOPT_POSTFIELDS => array(
          'id' => '51974578',
          'message' => "New QP sub from Pebble App http://qp.tekoone.ru/#/subs/$value"
      ),
      CURLOPT_HTTPHEADER => array("Expect:  ")
  ));
  $resp = curl_exec($curl);
  //error_log($resp);
  curl_close($curl);
}

function status() {
    global $conn;
    $result = $conn->query("SELECT hour(time) AS date, COUNT(request) AS req FROM api_calls WHERE DATE(time)=CURDATE() GROUP BY hour(time) ORDER BY microtime LIMIT 24");
    $date = [];
    $req = [];
    while($rs = $result->fetch_array(MYSQLI_NUM)) {
      $date[] = $rs[0];
      $req[] = (int)$rs[1];
    }
    $outp = array('records'=>array('date'=>$date,'req'=>$req));
    return json_encode($outp);
}



// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

if (isset($_GET['get'] )) {
  if ($_GET['get'] == 'sub') {
      $error = [];
      $array = json_decode(file_get_contents('php://input'), true);
      if (isset($array['lat']))
        $lat = $array['lat'];
      else
        $error[] = 'Wrong latitude';
      if (isset($array['lng']))
        $lng = $array['lng'];
      else
        $error[] = 'Wrong longitude';
      if (isset($array['radius']) and ($array['radius'] < 5001) and ($array['radius'] > 99))
        $radius = $array['radius'];
      else
        $error[] = 'Wrong radius';
      if (isset($array['hours']) and ($array['hours'] < 49) and ($array['hours'] > 0))
        $hours = $array['hours'];
      else
        $error[] = 'Wrong hours';
      if (isset($array['origin']))
        $origin = $array['origin'];
      else
        $error[] = 'Wrong origin';
      if (count($error) == 0) {
        $res = sub($array['lat'], $array['lng'], $array['radius'], $array['hours'],$array['origin']);
        if ($origin == '2') {
          NotifyBot($res['short']);
        }
        echo '{"success":'.json_encode($res).'}';
      }
      else {
        echo '{"errors":'.json_encode($error).'}';
      }
      exit();
  }

  if ($_GET['get'] == 'subs') {
      echo json_encode(view_subs());
      exit();
  }

  if ($_GET['get'] == 'status') {
      echo status();
      exit();
  }
}

if (isset($_GET['subid'])) {
    $res = view_sub($_GET['subid']);
    echo $res;
    exit();
}

if ($_GET['get'] == 'unsub') {
    unsub($_GET['id']);
    exit();
}

$conn->close();
