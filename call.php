<?
    require ('mysql.php');

    if ($_GET['hub_challenge']) {
        echo $_GET['hub_challenge'];
        exit();
    }

    if (file_get_contents("php://input")) {
        $updates = json_decode(file_get_contents("php://input"));
        foreach ($updates as $update) {
            $sql = "INSERT INTO updates (subscription_id, object, object_id, changed_aspect, time)
              VALUES ($update->subscription_id, '$update->object', '$update->object_id', '$update->changed_aspect', $update->time)";

            if ($conn->query($sql) === TRUE) {
                //echo "New record created successfully";
            } else {
                echo "Error: " . $sql . "<br>" . $conn->error;
            }
        }
    }

    $conn->close();